import os

from telebot import TeleBot
from telebot.types import Message

BOT_ACCESS_TOKEN = os.getenv("BOT_ACCESS_TOKEN", default="")

bot = TeleBot(token=BOT_ACCESS_TOKEN)


@bot.message_handler(commands=["start"])
def start_message(message: Message):
    bot.send_message(
        chat_id=message.chat.id,
        text="ЭТО СТАРТОВОЕ СООБЩЕНИЕ",
    )


@bot.message_handler()
def echo_message(message: Message):
    bot.send_message(
        chat_id=message.chat.id,
        text=message.text,
    )
