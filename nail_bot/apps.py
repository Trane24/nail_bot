from django.apps import AppConfig


class NailBotConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'nail_bot'
