from django.http import HttpRequest, HttpResponse
from django.views.decorators.csrf import csrf_exempt

from loguru import logger
from telebot.types import Update

from nail_bot.settings import bot


@csrf_exempt
def webhook_handler(request: HttpRequest) -> HttpResponse:
    update = Update.de_json(request.body.decode())
    logger.debug(f"{update=}")

    bot.process_new_updates([update])
    return HttpResponse("OK")
