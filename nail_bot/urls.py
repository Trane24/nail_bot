from django.urls import path

from nail_bot import views

urlpatterns = [
    path("webhook/", views.webhook_handler, name="webhook_handler"),
]
